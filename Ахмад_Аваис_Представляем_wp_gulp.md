# Представляем WPGulp: простой в использовании шаблон Gulp для работы с WordPress

> Хотя эта статья по-прежнему полезна для новичков, она немного устарела. WPGulp во многом развился. Убедитесь, что вы прочитали официальную документацию в [WPGulp GitHub Repo](https://github.com/ahmadawais/wpgulp/)  - я бы попросил, чтобы вы отметили  репозиторий звездочкой, чтобы отслеживать обновления.


Итак, вы никогда не использовали [Gulp](http://gulpjs.com/) раньше? Я здесь, чтобы помочь вам начать. Да! Вы можете использовать WPGulp и задать столько вопросов - в комментариях ниже - сколько хотите. И я буду уверен, что вы начнете профессиональную разработку WordPress.

Я использую Gulp уже более двух лет. Gulp облегчает жизнь; это простая в использовании система сборки на основе JavaScript. Вы можете использовать Gulp для автоматизации частей вашего рабочего процесса.

Если вы являетесь подписчиком этого блога , то, возможно, вы столкнулись с  моим [расширенным рабочим процессом Gulp для WordPress тем](https://ahmadawais.com/my-advanced-gulp-workflow-for-wordpress-themes/) . С тех пор мой рабочий процесс сильно изменился, но вы все равно можете получить довольно хорошее представление о том, насколько мощный Gulp.


Я открываю исходники своего расширенного рабочего процесса gulp, который я использую при построении премиальных тем WordPress [http://t.co/evardVlS5O](http://t.co/evardVlS5O) [pic.twitter.com/wawo4z1qES](http://t.co/wawo4z1qES)

> - Ахмад Аваис (@MrAhmadAwais) 24 августа 2015 г.


Я написал о Gulp в августе этого года. К моему удивлению, то, что я считал простым `brain_dump()`, стало популярным постом в [WordPress сообществе](https://ahmadawais.com/my-vision-wordpress-collaboration-over-competition/). Большая часть отзывов была отличной, но, также, немало людей попросили версию этого поста для начинающих.

Пока я думал о создании шаблона для Gulp, один из моих друзей [Рой Сиван](https://twitter.com/royboy789) пригласил меня поговорить на тему ["Grunt vs. Gulp, что лучше подходит для вашего WordPress?"](http://www.thewpcrowd.com/podcast/episode-018-gulp-grunt-wordpress/) в подкасте под названием TheWPCrowd. Для того подкаста я создал этот шаблон и назвал его WPGulp.

## Представляю [WPGulp](https://github.com/ahmadawais/WPGulp)


> Убедитесь, что вы подписались на обновления репозитория [WPGulp GitHub Repo](https://github.com/ahmadawais/WPGulp)!

[WPGulp](https://github.com/ahmadawais/WPGulp)  это шаблон Gulp для ваших WordPress проектов (независимо от типа, т. е. работает как для плагинов WP, так и для тем). Сейчас он построен с учетом новичков, но в конечном итоге у меня есть планы поместить больше веток в этот репозиторий с портируемыми задачами для Gulp. Итак, идите, отметьте звездочкой [этот репозиторий](https://github.com/ahmadawais/WPGulp). (Или следуйте за мной в [GitHub](https://github.com/ahmadawais/) ).

WPGulp - это простая реализация шаблона Gulp для WordPress. Он выполняет следующие задачи:

* Преобразование Sass в CSS
* Склейка (конкатенирование) JS
* Отслеживание изменений в файлах

#### Шаг № 0: Новичкам лучше начинать с этого!

Начнем с конфигурации. Поскольку мы обсуждаем Gulp здесь, я бы хотел избежать объяснений, связанных с Node, Terminal, Git и т. д. Когда я начал использовать инструменты для сборки, я ничего не знал об этих терминах, кроме их названий, конечно. Поэтому не беспокойтесь об этом. Как только вы dcе настроите, вы оглянетесь на свои старые проекты и будете громко смеяться над самими собой (Очень скоро?).

P.S, Я пользователь Mac, и все эти конфигурации предназначены для Mac. Но я уверен, что вы можете найти больше информации о настройках для Windows или Linux в Интернете.

#### Шаг №1: Установка Node и NPM (Node Packega Manager)

Прежде чем вы начнете использовать Gulp, вам нужно установить Node, который также установит NPM, т. е. Node Package Manager. Вам нужно установить его только один раз. Node.js можно загрузить для Mac, Windows и Linux по адресу [nodejs.org/download/](http://nodejs.org/download/) . Вы также можете [установить его из командной строки](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager) с помощью диспетчера пакетов.

После того, как вы установили Node, откройте терминал и введите:
`node -v`
Отобразится версия Node.js. Вы можете сделать то же самое для npm - диспетчера пакетов Node.js, который используется для установки модулей.
`npm -v`
Если какая-либо из команд не работает, проверьте, что вы набрали эту команду в нижнем регистре. Все еще не работает? Проверьте правильность установки Node.js.

#### Шаг №2: Давайте установим Gulp

Прежде чем использовать Gulp в проекте, его необходимо установить глобально (в вашей ОС). Вы можете установить его с помощью следующей команды.
`sudo npm install gulp -g`
Где `-g` указывает , что пакет Gulp устанавливается глобально. Теперь убедитесь, что Gulp был установлен следующей командой:
`gulp -v`

#### Шаг №3: Настройка WordPress проекта

Перейдите в папку вашего WordPress проекта (плагина или темы) и скопируйте файлы `gulpfile.js` и `package.json` в корневой каталог. Или, если вы начинаете новый проект, вы можете просто склонировать репозиторий WPGulp и начинать с ним. Как на скриншоте выше, я создал папку `WP-Project` и склонировал в нее репозиторий WPGulp.

#### Шаг 4: Разберем файл `package.json`

Package.json - это файл, который используется NPM для управления пакетами для вашего проекта. Он создается в проекте после выполнения команды `npm init`. После глобальной установки Gulp и создания файла `package.json` с помощью команды `npm init` вы можете установить gulp с помощью команды  `npm install gulp --save-dev`. Она установит Gulp и автоматически сохранит его в файле `package.json`.

Но с WPGulp вы можете игнорировать все это, так как вы получаете предварительно созданный файл `package.json`.

`{`
`  "name"      : "WPGulp",`
`  "author"    : "ahmadawais",`
`  "version"   : "1.0.0",`
`  "license"   : "GPL-2.0",`
`  "repository": {`
`    "type": "git",`
`    "url": "[https://github.com/ahmadawais/WPGulp](https://github.com/ahmadawais/WPGulp)"`
`  },`
`  "dependencies"   : {},`
`  "devDependencies": {`
`    "gulp"                      : "^3.8.11",`
`    "gulp-autoprefixer"         : "^3.1.0",`
`    "gulp-concat"               : "^2.5.2",`
`    "gulp-notify"               : "^2.2.0",`
`    "gulp-rename"               : "^1.2.0",`
`    "gulp-sass"                 : "^2.0.0",`
`    "gulp-sourcemaps"           : "^1.5.2",`
`    "gulp-uglify"               : "^1.1.0",`
`    "gulp-uglifycss"            : "^1.0.4"`
`  }`
`}`

Файл `package.json` в WPGulp достаточно хорошо самодокументирован. Так, связанные с проектом детали, которые вам нужно отредактировать, расположены со строки #1 до строки #9. После этого есть раздел devDependencies, в который не нужно вносить изменения, со строки #10 до строки #22. Вам не нужно ничего редактировать после строки #9. Все это плагины gulp, которые мы будем устанавливать в нашем проекте.

Итак, после того, как файлы  `gulpfile.js` и `package.json` добавлены в корневую папку вашего WP проекта, выполните следующую команду.
`sudo npm install`
Это создаст папку `node_modules` (где находятся Gulp и его плагины) в папке вашего WordPress проекта (в моем случае это папка WP-Project). На приведенном выше скриншоте показан результат установки без каких-либо ошибок. Вы можете игнорировать предупреждения `warning` (если они отображаются в терминале во время установки).

#### Шаг №5: Понимание Gulp

Для работы, Gulp требуется файл `gulpfile.js` в корневой папке вашего проекта. Я начал использовать Gulp только потому, что его подход более осмысленен - описывать конфигурацию кодом (поскольку Grunt имеет синтаксис JSON, а Gulp - JavaScript).

API Gulp содержит только **4 функции верхнего уровня**:

* `gulp.task` Определяет ваши задачи.
* `gulp.src` Указывает на файлы, которые вы хотите использовать, т.е. источник (source).
* `gulp.dest` Указывает на папку вывода, то есть назначения (destination).
* `gulp.watch` Помогает отслеживать изменения в файлах, т. е. Смена файла может вызвать конкретную задачу. Например, изменение файла CSS может вызвать задачу `styles`.

Вот и все. Хотя, не совсем, теперь вы знаете почти все, что есть в Gulp. Подумайте о Gulp как о потоке данных; вы можете вводить что угодно в любом месте потока и делать вывод что угодно в любом месте (Запутанно? Не обращайте внимания). `gulp.src` использует `.pipe` для объединения своего вывода.

#### Шаг # 6: Разберемся с файлом `gulpfile.js`

Давайте посмотрим на файл `gulpfile.js`, присутствующий в WPGulp. Я создал портативный файл, который можно использовать с любым WordPress проектом. Он состоит из следующих частей:

* **Configuration:**  конфигурация проекта для gulp задач.
* **Load Plugins:**  загрузка плагинов gulp и назначение им семантических имен.
* **Задача **`**styles**`**:  **Компилирует Sass, автоматически добавляет префиксы и минифицирует CSS.
* **Задача  **`**vendorJS**`**:** объединяет и минифицирует (с помощью uglify) JS скрипты сторонних поставщиков (плагинов).
* **Задача **`**customJS**`**:**  объединяет и минифицирует (с помощью uglify) пользовательские скрипты JS.
* **Задача отслеживания изменений в файлах **`**default**`**:**  Следит за изменениями файлов и запускает определенные задачи.

##### # 1: Configuration

В этой части вы можете задать, где находятся ваши файлы, и где вы хотите сохранять выходные файлы. Также здесь можно указать, за изменениями каких файлов должен следить Gulp. Прочтите комментарии, чтобы понять приведенный ниже код.

`/**`
`* Configuration.`
`** Project Configuration for gulp tasks.`
`** Edit the variables as per your project requirements.`
`*/`
`var project             = 'WPGulpTheme'; // Name`
`var styleSRC            = './assets/css/style.scss'; // Path to main .scss filevar styleDestination    = './'; // Path to place the compiled CSS file`
`// Default set to root folder`

`var jsVendorSRC         = './assets/js/vendors/*.js'; // Path to JS vendors foldervar jsVendorDestination = './assets/js/'; // Path to place the compiled JS vendors filevar jsVendorFile        = 'vendors'; // Compiled JS vendors file name`
`// Default set to vendors i.e. vendors.js`

`var jsCustomSRC         = './assets/js/custom/*.js'; // Path to JS custom scripts folder`
`var jsCustomDestination = './assets/js/'; // Path to place the compiled JS custom scripts file`
`var jsCustomFile        = 'custom'; // Compiled JS custom file name`
`// Default set to custom i.e. custom.js`
`var styleWatchFiles     = './assets/css/**/*.scss'; // Path to all *.scss files inside css folder and inside them`
`var vendorJSWatchFiles  = './assets/js/vendors/*.js'; // Path to all vendors JS files`
`var customJSWatchFiles  = './assets/js/custom/*.js'; // Path to all custom JS files`

##### # 2:  Load Plugins

Все загружаемые плагины, присутствуют в этой части. Можно было бы использовать  пакет [gulp-load-plugins](https://www.npmjs.com/package/gulp-load-plugins) для загрузки плагинов, но мне нравится контролировать, что подключено, а что нет, поэтому я предпочитаю включать плагины таким образом. Это также помогает при отладке при обновлении Node или любого другого пакета. Прочтите комментарии, чтобы понять приведенный ниже код.

`/** `
`* Load Plugins. `
`* `
`* Load gulp plugins and assign them semantic names. `
`*/`
`var gulp         = require('gulp'); // Gulp of-course`
`// CSS related plugins.`
`var sass         = require('gulp-sass'); // Gulp pluign for Sass compilation`
`var autoprefixer = require('gulp-autoprefixer'); // Autoprefixing magic`
`var minifycss    = require('gulp-uglifycss'); // Minifies CSS files`
`// JS related plugins.`
`var concat       = require('gulp-concat'); // Concatenates JS files`
`var uglify       = require('gulp-uglify'); // Minifies JS files`
`// Utility related plugins.`
`var rename       = require('gulp-rename'); // Renames files E.g. style.css -> style.min.css`
`var sourcemaps   = require('gulp-sourcemaps'); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)`
`var notify       = require('gulp-notify'); // Sends message notification to you`

##### # 3:  Задача `styles`
Убедитесь, что ваш СSS-файл со стилями WordPress темы начинается с` /**!` в  противном случае, `minifycss` может удалить комментарий WordPress в его верхней части. Это может сделать тему бесполезной, поскольку WP не признает ее в качестве темы. Вы можете проверить демоверсию WordPress темы WPGulp от [WPGulpTheme](https://github.com/ahmadawais/WPGulpTheme) . Кроме того, прочитайте комментарии, чтобы понять приведенный ниже код.

`/** `
`* Task: styles `
`* `
`* Compiles Sass, Autoprefixes it and Minifies CSS. `
`* `
`* This task does the following: `
`* 		1. Gets the source scss file `
`* 		2. Compiles Sass to CSS `
`* 		3. Writes Sourcemaps for it `
`* 		4. Autoprefixes it and generates style.css `
`* 		5. Renames the CSS file with suffix .min.css `
`* 		6. Minifies the CSS file and generates style.min.css `
`*/`
`gulp.task('styles', function () {`
` 	gulp.src( styleSRC )`
`		.pipe( sourcemaps.init() )`
`		.pipe( sass( {`
`			errLogToConsole: true,`
`			outputStyle: 'compact',`
`			//outputStyle: 'compressed',`
`			// outputStyle: 'nested',`
`			// outputStyle: 'expanded',`
`			precision: 10`
`		} ) )`
`		.pipe( sourcemaps.write( { includeContent: false } ) )`
`		.pipe( sourcemaps.init( { loadMaps: true } ) )`
`		.pipe( autoprefixer(`
`			'last 2 version',`
`			'> 1%',`
`			'safari 5',`
`			'ie 8',`
`			'ie 9',`
`			'opera 12.1',`
`			'ios 6',`
`			'android 4' ) )`
`		.pipe( sourcemaps.write ( styleDestination ) )`
`		.pipe( gulp.dest( styleDestination ) )`
`		.pipe( rename( { suffix: '.min' } ) )`
`		.pipe( minifycss( {`
`			maxLineLen: 10`
`		}))`
`		.pipe( gulp.dest( styleDestination ) )`
`		.pipe( notify( { message: 'TASK: "styles" Completed!', onLast: true } ) )`
`});`

##### # 4:  Задача `vendorJS`
Конкатенация (склеивание) Java-скриптов и их обезображивание ( уродование/`uglify`) могут ускорить загрузку страницы. Прочтите комментарии, чтобы понять приведенный ниже код.

`/** `
`* Task: vendorJS`
`* `
`* Concatenate and uglify vendor JS scripts. `
`* `
`* This task does the following: `
`* 		1. Gets the source folder for JS vendor files `
`* 		2. Concatenates all the files and generates vendors.js `
`* 		3. Renames the JS file with suffix .min.js `
`* 		4. Uglifes/Minifies the JS file and generates vendors.min.js `
`*/`
`gulp.task( 'vendorsJs', function() {`
`	gulp.src( jsVendorSRC )`
`		.pipe( concat( jsVendorFile + '.js' ) )`
`		.pipe( gulp.dest( jsVendorDestination ) )`
`		.pipe( rename( {`
`			basename: jsVendorFile,`
`			suffix: '.min'`
`		}))`
`		.pipe( uglify() )`
`		.pipe( gulp.dest( jsVendorDestination ) )`
`		.pipe( notify( { message: 'TASK: "vendorsJs" Completed!', onLast: true } ) );`
`});`

##### # 5:  Задача `customJS`
То же самое, конкатенация и `uglify` JavaScript могут помочь ускорить время загрузки страницы. Читайте комментарии, чтобы понять приведенный ниже код.

`/** `
`* Task: customJS `
`* `
`* Concatenate and uglify custom JS scripts. `
`* `
`* This task does the following: `
`* 		1. Gets the source folder for JS custom files `
`* 		2. Concatenates all the files and generates custom.js `
`* 		3. Renames the JS file with suffix .min.js `
`* 		4. Uglifes/Minifies the JS file and generates custom.min.js `
`*/`
`gulp.task( 'customJS', function() {`
` 	gulp.src( jsCustomSRC )`
`		.pipe( concat( jsCustomFile + '.js' ) )`
`		.pipe( gulp.dest( jsCustomDestination ) )`
`		.pipe( rename( {`
`			basename: jsCustomFile,`
`			suffix: '.min'`
`		}))`
`		.pipe( uglify() )`
`		.pipe( gulp.dest( jsCustomDestination ) )`
`		.pipe( notify( { message: 'TASK: "customJs" Completed!', onLast: true } ) );`
`});`

##### # 6:  Задача отслеживания изменений в файлах `default`
Наконец, задача `default`, которая вызовет все другие задачи в нашем файле. Прочтите комментарии, чтобы понять приведенный ниже код.

`/**  `
`* Watch Tasks.  `
`*  `
`* Watches for file changes and runs specific tasks.  `
`*/`
` gulp.task( 'default', [ 'styles', 'vendorsJs', 'customJS' ], function () {`
` 	gulp.watch( './assets/css/**/*.scss', [ 'styles' ] );`
` 	gulp.watch( './assets/js/vendors/*.js', [ 'vendorsJs' ] );`
` 	gulp.watch( './assets/js/custom/*.js', [ 'customJS' ] );`
` });`

Вот и все.

### Полный код `gulpfile.js`

Вот полный код файла `gulpfile.js`.

`/**`
` * Gulpfile.`
` *`
` * A simple implementation of Gulp.`
` *`
` * Implements:`
` * 			1. Sass to CSS conversion`
` * 			2. JS concatenation`
` * 			3. Watch files`
` *`
` * @since 1.0.0`
` * @author Ahmad Awais (@mrahmadawais)`
` */`
` /**`
`  * Configuration.`
`  *`
`  * Project Configuration for gulp tasks.`
`  *`
`  * Edit the variables as per your project requirements.`
`  */`
`var project             = 'WPGulpTheme'; // Name`
`var styleSRC            = './assets/css/style.scss'; // Path to main .scss filevar styleDestination    = './'; // Path to place the compiled CSS file// Defualt set to root folder`

`var jsVendorSRC         = './assets/js/vendors/*.js'; // Path to JS vendors folder`
`var jsVendorDestination = './assets/js/'; // Path to place the compiled JS vendors file`
`var jsVendorFile        = 'vendors'; // Compiled JS vendors file name`
`// Default set to vendors i.e. vendors.js`

`var jsCustomSRC         = './assets/js/custom/*.js'; // Path to JS custom scripts folder`
`var jsCustomDestination = './assets/js/'; // Path to place the compiled JS custom scripts file`
`var jsCustomFile        = 'custom'; // Compiled JS custom file name`
`// Default set to custom i.e. custom.js`
`var styleWatchFiles     = './assets/css/**/*.scss'; // Path to all *.scss files inside css folder and inside them`
`var vendorJSWatchFiles  = './assets/js/vendors/*.js'; // Path to all vendors JS files`
`var customJSWatchFiles  = './assets/js/custom/*.js'; // Path to all custom JS files`

`/**`
` * Load Plugins.`
` *`
` * Load gulp plugins and assing them semantic names.`
` */`
`var gulp         = require('gulp'); // Gulp of-course`
`// CSS related plugins.`
`var sass         = require('gulp-sass'); // Gulp pluign for Sass compilation`
`var autoprefixer = require('gulp-autoprefixer'); // Autoprefixing magic`
`var minifycss    = require('gulp-uglifycss'); // Minifies CSS files`
`// JS related plugins.`
`var concat       = require('gulp-concat'); // Concatenates JS files`
`var uglify       = require('gulp-uglify'); // Minifies JS files`
`// Utility related plugins.`
`var rename       = require('gulp-rename'); // Renames files E.g. style.css -> style.min.css`
`var sourcemaps   = require('gulp-sourcemaps'); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)`
`var notify       = require('gulp-notify'); // Sends message notification to you`

`/**`
` * Task: styles`
` *`
` * Compiles Sass, Autoprefixes it and Minifies CSS.`
` *`
` * This task does the following:`
` * 		1. Gets the source scss file`
` * 		2. Compiles Sass to CSS`
` * 		3. Writes Sourcemaps for it`
` * 		4. Autoprefixes it and generates style.css`
` * 		5. Renames the CSS file with suffix .min.css`
` * 		6. Minifies the CSS file and generates style.min.css`
` */`
`gulp.task('styles', function () {`
` 	gulp.src( styleSRC )`
`		.pipe( sourcemaps.init() )`
`		.pipe( sass( {`
`			errLogToConsole: true,`
`			outputStyle: 'compact',`
`			//outputStyle: 'compressed',`
`			// outputStyle: 'nested',`
`			// outputStyle: 'expanded',`
`			precision: 10`
`		} ) )`
`		.pipe( sourcemaps.write( { includeContent: false } ) )`
`		.pipe( sourcemaps.init( { loadMaps: true } ) )`
`		.pipe( autoprefixer(`
`			'last 2 version',`
`			'> 1%',`
`			'safari 5',`
`			'ie 8',`
`			'ie 9',`
`			'opera 12.1',`
`			'ios 6',`
`			'android 4' ) )`
`		.pipe( sourcemaps.write ( styleDestination ) )`
`		.pipe( gulp.dest( styleDestination ) )`

`		.pipe( rename( { suffix: '.min' } ) )`
`		.pipe( minifycss( {`
`			maxLineLen: 10`
`		}))`
`		.pipe( gulp.dest( styleDestination ) )`
`		.pipe( notify( { message: 'TASK: "styles" Completed!', onLast: true } ) )`
`});`

`/**`
` * Task: vendorJS`
` *`
` * Concatenate and uglify vendor JS scripts.`
` *`
` * This task does the following:`
` * 		1. Gets the source folder for JS vendor files`
` * 		2. Concatenates all the files and generates vendors.js`
` * 		3. Renames the JS file with suffix .min.js`
` * 		4. Uglifes/Minifies the JS file and generates vendors.min.js`
` */`
`gulp.task( 'vendorsJs', function() {`
`	gulp.src( jsVendorSRC )`
`		.pipe( concat( jsVendorFile + '.js' ) )`
`		.pipe( gulp.dest( jsVendorDestination ) )`
`		.pipe( rename( {`
`			basename: jsVendorFile,`
`			suffix: '.min'`
`		}))`
`		.pipe( uglify() )`
`		.pipe( gulp.dest( jsVendorDestination ) )`
`		.pipe( notify( { message: 'TASK: "vendorsJs" Completed!', onLast: true } ) );`
`});`

`/**`
` * Task: customJS`
` *`
` * Concatenate and uglify custom JS scripts.`
` * * This task does the following:`
` * 		1. Gets the source folder for JS custom files`
` * 		2. Concatenates all the files and generates custom.js`
` * 		3. Renames the JS file with suffix .min.js`
` * 		4. Uglifes/Minifies the JS file and generates custom.min.js`
` */`
`gulp.task( 'customJS', function() {`
` 	gulp.src( jsCustomSRC )`
`		.pipe( concat( jsCustomFile + '.js' ) )`
`		.pipe( gulp.dest( jsCustomDestination ) )`
`		.pipe( rename( {`
`			basename: jsCustomFile,`
`			suffix: '.min'`
`		}))`
`		.pipe( uglify() )`
`		.pipe( gulp.dest( jsCustomDestination ) )`
`		.pipe( notify( { message: 'TASK: "customJs" Completed!', onLast: true } ) );`
`});`
` /**`
`  * Watch Tasks.`
`  *`
`  * Watches for file changes and runs specific tasks.`
`  */`
` gulp.task( 'default', [ 'styles', 'vendorsJs', 'customJS' ], function () {`
` 	gulp.watch( styleWatchFiles, [ 'styles' ] );`
` 	gulp.watch( vendorJSWatchFiles, [ 'vendorsJs' ] );`
` 	gulp.watch( customJSWatchFiles, [ 'customJS' ] );`
` });`

##### Шаг № 7: Запуск Gulp

Наконец, после настройки `gulpfile.js` вам нужно запустить команду `gulp`, которая будет запускать задачу по умолчанию (`default`), которая по очереди запускает задачи `styles`, `vendorsJs`, `customJS` и, наконец, начинает отслеживание ваших файлов .scss и .js. Итак, вот команда:
`gulp`

### **Жду с нетерпением!**

Уф! Это было мощно. Жду ваших комментариев. Дайте мне знать, если я что-то пропустил или вы чего-то не поняли.

Вот ссылки на ресурсы, на всякий случай:

* [WPGulp](https://github.com/ahmadawais/WPGulp) : Gulp WordPress Boilerplate.
* [WPGulpTheme](https://github.com/ahmadawais/WPGulpTheme): вы можете посмотреть демо тему WordPress, созданную с помощью WPGulp.
* [Мой расширенный рабочий процесс Gulp для WordPress Темы](https://ahmadawais.com/my-advanced-gulp-workflow-for-wordpress-themes/)

## **ОБНОВЛЕНИЯ WPGulp:**

С того времени, как я написал эту статью, WPGulp сильно вырос, и теперь он поддерживает следующие задачи.

**Что может сделать WPGulp?**

* Живая перезагрузка (`Live Reload`) в браузере с помощью `BrowserSync`
* CSS: преобразование Sass в CSS, обнаружение ошибок, автоматическая расстановка вендорных префиксов (`autoprefixer`), карты источников (`sourcemaps`), минификация CSS и объединение media-запросов.
* JS: объединение и уродование JS-файлов, пользовательских и от сторонних поставщиков.
* Изображения: Минимизация изображений PNG, JPEG, GIF и SVG.
* Наблюдение за изменениями в CSS или JS файлах
* Наблюдение за изменениями в PHP файлах
* Корректировка символов окончания строки
* Инъекция CSS (`InjectCSS`) вместо перезагрузки страницы браузером
* Создание .pot файла для i18n ( internationalization - интернационализации) и l10n ( localization - локализации )

Хотя эта статья по-прежнему полезна для новичков, она немного устарела, так как WPGulp сильно развился. Убедитесь, что вы прочитали официальную документацию в [WPGulp GitHub Repo](https://github.com/ahmadawais/wpgulp/)  - я бы попросил, чтобы вы подписались репозиторий, чтобы не отставать от обновлений.